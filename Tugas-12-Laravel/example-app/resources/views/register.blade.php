<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1> Buat Account Baru!</h1>
    <h2>Sign Up Form</h2> 

    <form action="/kirim" method="post">
    @csrf 
        <label> First name: </label><br><br>
        <input type="text" name="fName"> <br> <br>
        <label> Last name:</label> <br><br>
        <input type="text" name="lName"><br> <br>
        <label> Gender:</label> <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other  <br> <br>
        <label> Nationality:</label><br><br>
        <select name="Nationality">
            <option value="1">Indonesia</option> 
            <option value="2">Malaysia</option> 
            <option value="3">Singapura</option> 
        </select> <br><br>
        <label> Language Spoken:</label> <br><br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br><br>
        <label> Bio: </label><br><br>
        <textarea cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up">

    </form>
</body>
</html>