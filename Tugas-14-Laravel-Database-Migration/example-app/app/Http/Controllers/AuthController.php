<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi()
    {
        return view('register');
    }

    public function send(request $request)
    {
        $namaDepan = $request['fName'];
        $namaBelakang = $request['lName']; 

        return view('welcome',['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
