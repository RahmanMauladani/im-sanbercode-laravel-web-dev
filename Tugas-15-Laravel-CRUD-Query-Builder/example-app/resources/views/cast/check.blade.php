@extends('layout.master')
@section('title')
<h1>Halaman Detail Data Cast!</h1>
@endsection
@section('sub-title')
<h2>Cast</h2> 
@endsection
@section('content')

<h2>{{$cast->nama}}</h2><br>
<h4>Umur = {{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btm btn-secondary btn-sm">Kembali</a>

@endsection