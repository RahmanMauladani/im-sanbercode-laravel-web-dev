@extends('layout.master')
@section('title')
<h1>Halaman Edit Cast!</h1>
@endsection
@section('sub-title')
<h2>Cast</h2> 
@endsection
@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @method('put')
        @csrf
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" class="form-control" value="{{$cast->nama}}"name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Umur</label>
            <input type="integer" class="form-control" value="{{$cast->umur}}" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection