@extends('layout.master')
@section('title')
<h1>Halaman Tambah Cast!</h1>
@endsection
@section('sub-title')
<h2>Cast</h2> 
@endsection
@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" class="form-control" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="exampleInputPassword1">Umur</label>
            <input type="integer" class="form-control" name="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" class="form-control"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection