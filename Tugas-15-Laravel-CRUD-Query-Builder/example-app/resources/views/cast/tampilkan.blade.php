@extends('layout.master')
@section('title')
<h1>Halaman List Data Cast!</h1>
@endsection
@section('sub-title')
<h2>Cast</h2> 
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah Cast</a>

<table class="table ">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Detail</th>
      </tr>
    </thead>

    <tbody>
      @forelse ($cast as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>             
              <form action="/cast/{{$item->id}}" method="POST">
                @method('delete')
                @csrf
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Check</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm"  value="Delete">
              
              </form>
            </td>
          </tr>
      @empty
          <tr>
            <td>Data Cast Kosong</td>
          </tr>
      @endforelse

    </tbody>
  </table>
@endsection