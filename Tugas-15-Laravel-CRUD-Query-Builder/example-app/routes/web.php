<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController; 
use App\Http\Controllers\AuthController; 
use App\Http\Controllers\CastController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'halDepan']);
Route::get('/register', [AuthController::class, 'registrasi']);

Route::post('/kirim', [AuthController::class, 'send']);

Route::get('/data-table', function(){
    return view('tabeldata');
});

Route::get('/table', function(){
    return view('tabel');
});

//CRUD CAST
//Create
Route::get('/cast/create', [CastController::class, 'create']);
//Route menyimpan data ke database
Route::post('/cast', [CastController::class, 'store']);

//Read data cast
Route::get('/cast', [CastController::class, 'index']);

//Route data Check
Route::get('/cast/{id}', [CastController::class, 'show']);

//Update data cast
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route untuk Update data
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete cast berdasarkan id-nya
Route::delete('/cast/{id}', [CastController::class, 'destroy']);