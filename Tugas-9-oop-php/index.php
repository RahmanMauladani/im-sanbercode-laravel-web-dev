<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    echo "<h3>Output Akhir</h3>";
 
        $sheep = new Animal("shaun");

        echo "Name : " . $sheep->name . "<br>"; 
        echo "Legs : " . $sheep->legs . "<br>"; 
        echo "Cold Blooded : " . $sheep->cold_blooded . "<br> <br>"; 

        $kodok = new Frog("buduk");
        echo "Name : " . $kodok->name . "<br>"; 
        echo "Legs : " . $kodok->legs . "<br>"; 
        echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; 
        $kodok->jump(); 
        echo "<br><br>";

        $sungokong = new Ape("kera sakti");
        echo "Name : " . $sungokong->name . "<br>"; 
        echo "Legs : " . $sungokong->legs . "<br>"; 
        echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
        $sungokong->yell();

        
?>